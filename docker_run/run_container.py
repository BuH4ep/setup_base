
import docker
import standart_functions as sf


def run_container():
    client = docker.from_env()
    client.containers.run(image='mockserver/mockserver', name='mock_server', remove=True,
                          ports={'1080/tcp': 1080}, detach=True)
    container = client.containers.get('mock_server')
    sf.check_container('http://127.0.0.1:1080')
    return container


def stop_container(container):
    container.stop()



def check_container(url, wait=30):
    start_time = time.time()
    result = False
    while (time.time() - start_time) < wait and (result is False):
        try:
            requests.get(url)
            result = True
            return result
        except requests.exceptions.ConnectionError:
            result = False
    assert result, f'Контейнер не поднялся за {wait} секунд'
