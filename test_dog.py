import pytest
import allure
# from selenium import webdriver
import time

print('Heoolo')
# @allure.story("Some story")
# @allure.feature("Sample suite")
# class Tes@allure.story()
@allure.story("Start")
@allure.step("Test case 1")
    # @allure.testcase("http://my.tms.org/TESTCASE-1")
@allure.severity(allure.severity_level.BLOCKER)
def test_simple():
    with allure.step('Go to 1'):
        try:
            a = 0 / 0
            b = 2-5
            print(b)
        except ZeroDivisionError:
            print('Haha')
    with allure.step('GO GO 2'):
        print('EROOR')

@allure.story("Finish")
@allure.step("Test case 2")
    # @allure.testcase("http://my.tms.org/TESTCASE-2")
@allure.severity(allure.severity_level.CRITICAL)
def test_simple_1():
    allure.step('Go to 2')
    print('test 2 ')



import allure
import pytest

@pytest.fixture
def attach_file_in_module_scope_fixture_with_finalizer(request):
    allure.attach('A text attacment in module scope fixture', 'blah blah blah', allure.attachment_type.TEXT)
    def finalizer_module_scope_fixture():
        allure.attach('A text attacment in module scope finalizer', 'blah blah blah blah',
                      allure.attachment_type.TEXT)
    request.addfinalizer(finalizer_module_scope_fixture)


def test_with_attacments_in_fixture_and_finalizer(attach_file_in_module_scope_finalizer):
    pass


@pytest.mark.parametrize("num", [1, 6, 5, 4,])
def test_num(num):
    with allure.step("Считаем числа"):
        print(num)


def test_multiple_attachments():
    allure.attach.file('d:/скрины/bonus_add.png', attachment_type=allure.attachment_type.PNG)
    allure.attach('<head></head><body> a page </body>', 'Attach with HTML type', allure.attachment_type.HTML)
#
# # def decorator_function(wrapped_func):
# #     def wrapper():
# #         print('Входим в функцию-обёртку')
# #         print('Оборачиваемая функция: ', wrapped_func)
# #         print('Выполняем обёрнутую функцию...')
# #         wrapped_func()
# #         print('Выходим из обёртки')
# #     return wrapper
# #
# #
# # @decorator_function
# # def hello_world():
# #         print('Hello world!')
# # hello_world()
