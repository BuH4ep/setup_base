import docker
from docker import constants
import os
import time


def generation(par_dir, par_dir_report):
    """Функция по генерации allure отчета в html формат через docker"""
    # if not os.path.exists('allure_report'):
    #     os.mkdir('allure_report')
    client = docker.from_env()
    client.containers.run(image='frankescobar/allure-docker-service',
                          name='allure',
                          remove=True,
                          ports={'5050/tcp': 5050},
                          detach=True,
                          volumes=[
                              rf'{par_dir}:/app/allure-results',
                              rf'{par_dir_report}:/app/default-reports'
                          ])
    container = client.containers.get('allure')
    # time.sleep(15)
    log = container.logs()
    # print(log)
    container.stop()
# g:\ALL\URIS\iburyy\
# C:/Program Files/Python37/my_scripts/GIT/setup_base/allure-report  d:\artifacts (4)\allure-results\ C:/Program Files/Python37/my_scripts/GIT/oat_moby-2.0/allure-results
generation('d:/artifacts (4)/allure-results', 'C:/Program Files/Python37/my_scripts/GIT/setup_base/allure-report')

# from combine import combine_allure

# combine_allure('C:/Program Files/Python37/my_scripts/GIT/setup_base/allure-report/latest')