import inspect
import logging
import os
import re
import time

import allure
import pyperclip
from allure_commons.types import AttachmentType
from selenium.common.exceptions import NoSuchElementException


def select_path_apk():
    """Поиск apk"""
    path = inspect.getabsfile(select_path_apk)
    for root, dirs, files in os.walk(path + "/../"):
        for name in files:
            if '.apk' in name:
                name = os.path.join(root, name)
                return name


def desired_caps(app_path):
    """desired_caps for run app on emulator"""
    desired = {"platformName": "Android",
               "deviceName": "Test",
               "app": f"{app_path}",
               "newCommandTimeout": 6000,
               # "unicodeKeyboard": True,
               # "resetKeyboard": True,
               "appWaitForLaunch": False,
               "appWaitActivity": '*'
               }
    return desired

def find_element(driver, id='', xpath='', text='', wait=30):
    """Функция поиска элемента"""

    time_start = time.time()
    find_result = False
    if text:
        while (time.time() - time_start) < wait and find_result is False:
            try:
                elem = driver.find_element_by_xpath(f"//android.widget.TextView[@text='{text}']")
                elem_display = driver.find_element_by_xpath(
                    f"//android.widget.TextView[@text='{text}']").get_attribute('displayed')
                return elem
            except:
                pass
    elif id:
        while (time.time() - time_start) < wait and find_result is False:
            try:
                elem = driver.find_element_by_id(id)
                return elem
            except:
                pass
    elif xpath:
        while (time.time() - time_start) < wait and find_result is False:
            try:
                elem = driver.find_element_by_xpath(xpath)
                return elem
            except:
                pass
    else:
        assert False
    try:
        assert find_result is True, 'Элемент не найден'
    except AssertionError:
        allure.attach(driver.get_screenshot_as_png(), 'screen_error', attachment_type=AttachmentType.PNG)
        raise AssertionError('Элемент не найден')


def click_element(driver, elem, wait=30):
    """Функция клика"""

    time_start = time.time()
    result = False
    while (time.time() - time_start) < wait and result is False:
        try:
            elem.click()
            result = True
            time.sleep(1)
        except:
            pass
    try:
        assert result is True, 'Элемент не найден'
    except:
        allure.attach(driver.get_screenshot_as_png(), 'screen_error', attachment_type=AttachmentType.PNG)
        raise AssertionError('Элемент не найден')


def input_text(driver, elem, text):
    """Функция для ввода текста в текстовое поле"""
    try:
        elem.send_keys(text)
    except:
        allure.attach(driver.get_screenshot_as_png(), 'screen_error', attachment_type=AttachmentType.PNG)
        raise AssertionError('Элемент не найден')


def paste_buffer(driver, value):
    """Вставка из буфера значения переменной"""
    pyperclip.copy(value)
    print(value)
    driver.press_keycode(279)


def find_elemets_xpath(driver, xpath=''):
    """Опереление колиество элементов по xpath"""
    count = str(len(driver.find_elements_by_xpath(xpath)))
    return count


def setup_geo_location(coord):
    """Установка геолокации на емуляторе локально"""
    for root, dirs, files in os.walk('c:/users'):
        for name in files:
            if 'adb.exe' in name and 'Sdk' in root and 'AppData' not in root:
                os.system(f'cd {root} && {coord}')
