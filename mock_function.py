import requests
import requests_mock

body = {
                "cardList": [
                    {"cardReferenceId": "eb5b91af-9aa50b60d2d9d620",
                     "lastNumbers": "2455",
                     "expMonth": "05",
                     "expYear": "25",
                     "isDefault": "false"
                     },
                    {
                        "cardReferenceId": "1af-9aa50b60d2d",
                        "lastNumbers": "4789",
                        "expMonth": "08",
                        "expYear": "23",
                        "isDefault": "true"
                    }
                ]
            }
url = 'http://localhost:1081/api/v1/card-storage/cards/+375447231959'

@requests_mock.Mocker()
def mock_function(m, url, json):
    m.get(url=url, json=json, status_code=200, headers={
                'Content-type': 'application/json',
                "Accept": "*/*"
            })
    return requests.get(url)

# print(mock_function(url=url, json=body).headers)
def card_storage_cards():
    requests.put('http://localhost:1080/mockserver/expectation', json={
        "httpRequest": {
            "method": 'GET',
            "path": '/api/v1/card-storage/cards/+375447231959'},
        "httpResponse": {
            "headers": {
                'Content-type': ['application/json'],
                "Accept": ["*/*"]
            },
            "statusCode": 200,
            "body": {
                "cardList": [
                    {"cardReferenceId": "eb5b91af-9aa50b60d2d9d620",
                     "lastNumbers": "2455",
                     "expMonth": "05",
                     "expYear": "25",
                     "isDefault": "false"
                     },
                    {
                        "cardReferenceId": "1af-9aa50b60d2d",
                        "lastNumbers": "4789",
                        "expMonth": "08",
                        "expYear": "23",
                        "isDefault": "true"
                    }
                ]
            }
        }
    })
    response = requests.get('http://localhost:1080/api/v1/card-storage/cards/+375447231959')
    return response
    # return response

def get_3ds_pagea():
    requests.put('http://localhost:1080/mockserver/expectation', json={
        "httpRequest": {
            "headers": {
                "Host": ["https://mp-dev.cotvec.com"],
                "path": "/api/v1/geo/search/dot?lat=53.898331&lng=27.560555"
            }},
        "httpResponse": {
            "headers": {"Content-Type": ["application/json"]},
            "body": {
                "lat": 53.89835830000001,
                "lng": 27.5605372,
                "formatted_location": "Ленину 20, г/п Комарин, Брагин, 247650, Минск, Беларусь"
            }
        }
    })
