def health_person(health):
    if health <= 0:
        print('False')
    else:
        print('True')


def number():
    num = input('Enter number: ')
    if int(num) % 2 == 0:
        print(f'Number {num} is even')
    else:
        print(f'Nuber {num} is not even')


def leap_year():
    year = input('Enter year: ')
    if int(year) % 4 == 0 and int(year) > 999:
        print(f'Year is leap')
    else:
        print(f'Year is not leap')


def input_text():
    number = input("Enter number for output text some count: ")
    for i in range(int(number)):
        print('Hello it is text')


class ExceptionPrintSendData(Exception):
    AssertionError('Error')


def func():
    a = 5
    b = 0
    try:
        c = a / b
        return c
    except ZeroDivisionError as error:
        raise Exception(error)

# func()


print('Hi\xa0\xa0lox')