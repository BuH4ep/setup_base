import os

import pytest
import standart_function as sf
from appium import webdriver


def pytest_addoption(parser):
    parser.addoption(
        "--browser",
        choices=['local', 'docker'],
        default="local",
        help="This is type of browser"
    )
    parser.addoption(
        "--path",
        default="local",
        help="This is type of path"
    )


@pytest.fixture(scope='session')
def browser_type(request):
    return request.config.getoption("--browser")


@pytest.fixture(scope='session')
def apk_path(request):
    return request.config.getoption("--path")


@pytest.fixture(scope='function', autouse=False)
def start_driver(browser_type, apk_path):
    if browser_type == 'docker':
        start_driver = webdriver.Remote(command_executor='http://localhost:4723/wd/hub',
                                        desired_capabilities=sf.desired_caps(apk_path))
        print(start_driver.session)
        yield start_driver
        start_driver.quit()
    elif browser_type == 'local':
        start_driver = webdriver.Remote('http://localhost:4723/wd/hub', sf.desired_caps(sf.select_path_apk()))
        yield start_driver
        start_driver.quit()

#
# @pytest.fixture(scope='module')
# def driver(browser_type, apk_path):
#     if browser_type == 'docker':
#         driver = webdriver.Remote(command_executor='http://localhost:4723/wd/hub',
#                                   desired_capabilities=sf.desired_caps(apk_path))
#         sf.login_client_mb(driver)
#         yield driver
#         sf.terminate_app(driver)
#         driver.quit()
#     elif browser_type == 'local':
#         driver = webdriver.Remote('http://localhost:4723/wd/hub', sf.desired_caps(sf.select_path_apk()))
#         sf.login_client_mb(driver)
#         yield driver
#         sf.terminate_app(driver)
#         driver.quit()
#
#
# @pytest.fixture(scope='module')
# def start_driver_session(browser_type, apk_path):
#     if browser_type == 'docker':
#         start_driver_session = webdriver.Remote(command_executor='http://localhost:4723/wd/hub',
#                                                 desired_capabilities=sf.desired_caps(apk_path))
#         yield start_driver_session
#         sf.terminate_app(start_driver_session)
#         start_driver_session.quit()
#     elif browser_type == 'local':
#         start_driver_session = webdriver.Remote('http://localhost:4723/wd/hub', sf.desired_caps(sf.select_path_apk()))
#         yield start_driver_session
#         sf.terminate_app(start_driver_session)
#         start_driver_session.quit()


@pytest.fixture(scope='function', autouse=True)
def preconditions(request):
    """Название теста с расширение png для скриншота"""
    test_name = request.node.name + '.png'
    try:
        os.remove(f'../screenshot/{test_name}')
    except:
        pass
    return test_name


@pytest.hookimpl(hookwrapper=True, tryfirst=True)
def pytest_runtest_makereport(item, call):
    outcome = yield
    rep = outcome.get_result()
    setattr(item, "rep_" + rep.when, rep)
    return rep
