import requests
import json
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def get_client_token():
    form_headers = {'accept': 'application/json', 'content-type': 'application/x-www-form-urlencoded'}
    headers_type = form_headers
    app_auth = ('payforce_MB', 'ak8!6k@1')
    url = 'https://93.125.98.117:8443/api/oauth/token'
    app_token = {'grant_type': 'client_credentials'}
    client_token = requests.post(url, app_token, auth=app_auth, verify=False).json()
    client_token = client_token['access_token']
    headers = headers_type
    headers['Authorization'] = f'Bearer {client_token}'
    return headers


import random

def autho_client_doc():
    headers = get_client_token()
    client_token = requests.get("https://93.125.98.117:8443/api/oauth/login/otp_sms/challenge?userName=%2b375297612114", verify=False).json()
    print(client_token)



def deviceFingerprint():
    finget_random = random.randint(1, 100000)
    deviceDescription = {'appVersion': '1.0.34',
                         'brand': 'Android',
                         'deviceId': 'e72e1e974845746a',
                         # 'deviceId': f'{finget_random}',
                         'deviceModel': 'Samsung Galaxy S8',
                         'osType': 'Android',
                         'osVersion': '9'}
    rb = {
        "deviceAuthType": "PIN",
        "deviceDescription": f"{deviceDescription}"
    }

    headers = get_client_token()
    finger = requests.post(f'https://93.125.98.117:8443/api/oauth/login/token/{finget_random}', json=rb,
                           headers=headers, verify=False).json()
    print(finger)


autho_client_doc()
