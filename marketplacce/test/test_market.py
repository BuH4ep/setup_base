from marketplacce.src.geo_page import GeoPage


def get_3ds_pagea():
    requests.put('http://localhost:1080/mockserver/expectation', json={
        "httpRequest": {
            "headers": {
                "Host": ["https://mp-dev.cotvec.com"],
                "path": "/api/v1/geo/search/dot?lat=53.898331&lng=27.560555"
            }},
        "httpResponse": {
            "headers": {"Content-Type": ["application/json"]},
            "body": {
                "lat": 53.89835830000001,
                "lng": 27.5605372,
                "formatted_location": "Ленину 20, г/п Комарин, Брагин, 247650, Минск, Беларусь"
            }
        }
    })


def test_resolution(start_driver):
    geo_page = GeoPage(start_driver)
    geo_page.geo_resolution()
    get_3ds_pagea()
    geo_page.enter_addres_delivery('Klumova')


def test_not_resolution(start_driver):
    geo_page = GeoPage(start_driver)
    geo_page.geo_not_resolution()
    print('yes')
