import time

import standart_function as sf




class Driver(object):
    """Get driver application"""
    def __init__(self, driver):
        self.driver = driver


class GeoPage(Driver):
    def geo_resolution(self):
        elem = sf.find_element(self.driver, id='com.android.permissioncontroller:id/permission_allow_foreground_only_button')
        sf.click_element(self.driver, elem)

    def geo_not_resolution(self):
        elem = sf.find_element(self.driver, id='com.android.permissioncontroller:id/permission_deny_button')
        sf.click_element(self.driver, elem)

    def enter_addres_delivery(self, address: str):
        elem = sf.find_element(self.driver, xpath='//android.widget.TextView[1]')
        sf.click_element(self.driver, elem)
        elem = sf.find_element(self.driver, xpath='//android.widget.EditText[@index="2"]')
        sf.input_text(self.driver, elem, address)